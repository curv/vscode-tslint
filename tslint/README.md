# vscode-tslint-extended
⚠️ Bad Quality Extension, still in Development! ⚠️
# Information about this Extension
This is a fork of [vscode-tslint](https://github.com/Microsoft/vscode-tslint).
The goal of this fork is, to enable Rules that require typing-informations, which is not possible with the original Extension. See [Issue #70](https://github.com/Microsoft/vscode-tslint/issues/70) for vscode-tslint

## Quality of this Extension
This is (for now) just a quick hack of the Original Extension.

We modifed the tslint-server to run a full project lint on each save.
For now, we'll just try to execute the file `./node_modules/tslint/bin/tslint` in the Project and parse the output.

This extension was tested on Linux Systems, we have no clue if this will work on windows.

Also we have no experience on how to fork packages and later create Pull-requests.
Any Feedback is welcome. - [Contact Page](https://reach-rocket.de/kontakt/)